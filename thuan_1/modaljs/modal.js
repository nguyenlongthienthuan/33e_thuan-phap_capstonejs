var card_item=(product,index)=>{
    return{
        product:product,
        index:index,
    }
};
var item_product=(product,index)=>{
    var content='';
    var logo_type='';
  let vtri=  list_buy.findIndex((item)=>{return(item.product.id==product.id)});
    var icon_buy=`
    <div style="display:${(vtri!=-1)?"block":"none"}" id="icon_buy">
    <i onclick="main('down','${product.id}','${index}')" class="fa fa-chevron-circle-left"></i>
    <span id="soluong">${(vtri!=-1)?list_buy[vtri].index:"1"}</span>
    <i onclick="main('up','${product.id}','${index}')" class="fa fa-chevron-circle-right"></i>
    </div>
    `;
    let stt_item_buy=(vtri!=-1)?`${vtri}`:"";
    if(product.type=="Iphone"){
       logo_type='<i class="fab fa-apple"></i>';
    }else{
       logo_type='<i class="fab fa-android"></i>';
    }
    content= /*html*/`
   <section id="id_${product.id}" class="product_item">
      <div class="container">   
      <div class="product_logo">${logo_type}</div>
      <div class="product_img"><img src="${product.img}" alt=""></div>
      <div class="product_info">
       <div class="over_play_product_item">
       <nav>
       <ul>
         <li><h3>${product.name}</h3> <i class="fa fa-heart"></i></li>
         <li>${product.desc}</li>
         <li>${product.screen}</li>
         <li>${product.backCamera}</li>
         <li>${product.frontCamera}</li>
         <li>${product.price}</li>
         <li style="text-align: right">
         <i style="display:${(vtri!=-1)?"none":"block"}" onclick="main('add','${product.id}','${index}')" class="fa fa-shopping-basket "></i> 
         ${icon_buy}
         </li>
       </ul>
       </nav>
        </div>
      </div>
      </div>
    </section>
    `
return content;
}
var item_buy=(buy_list,index)=>{
      let  content=`
           <div id="id_${buy_list.product.id}" class="card_row">
            <div style="display:none" id="stt">${index}</div>
           <ul>
           <li><img src="${buy_list.product.img}" alt=""></li>
           <li>${buy_list.product.name}</li>
           <li> 
          <div style="display:block" id="icon_buy">
          <i onclick="main('down','${buy_list.product.id}','${index}')" class="fa fa-chevron-circle-left"></i>
          <span id="soluong">${buy_list.index}</span>
          <i onclick="main('up','${buy_list.product.id}','${index}')" class="fa fa-chevron-circle-right"></i>
          </div>
           </li>
           <li id="price">${buy_list.product.price*buy_list.index}</li>
           <li><i onclick="main('delete','${buy_list.product.id}','${index}')"  class="fa fa-trash-alt"></i></li>
           </ul>
           </div>
        `;
     return content;
 }
 