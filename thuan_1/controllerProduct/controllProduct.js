let render = (list, item_modal,id_html) => {
  let content_html = ``;
  let render_array = () => {
    list.forEach((item_list, index) => {
      content_html += item_modal(item_list,index);
    });
    document.getElementById(id_html).innerHTML = content_html;
  }
  let render_item = () => {
    document.getElementById(id_html).insertAdjacentHTML("beforeend", item_modal(list,list_buy.length-1 ));
  }
  (Array.isArray(list)) ? render_array() : render_item();
}
function add_shopping(id, index, so_luong) {
  let index_item_buy= (!document.querySelector(`.card_container #id_${id} #stt`))?-1:document.querySelector(`.card_container #id_${id} #stt`).innerHTML;
  (index_item_buy!=-1)?list_buy[index_item_buy].index=so_luong:(list_buy.push(card_item(product_list[index],1)),render(card_item(product_list[index], so_luong), item_buy, "card_container"));
  show_hide_btn_up_down("none","block",id,so_luong);
  save_Storage(list_buy);
}   
function up(id, index, so_luong) {
  tinh_tang_giam(id, index, so_luong + 1);
}
function down(id, index, so_luong) {
  tinh_tang_giam(id, index, so_luong - 1);
}
function tinh_tang_giam(id, index, so_luong) {
  list_buy[document.querySelector(`#card_container #id_${id} #stt`).innerHTML*1].index=so_luong;
 (so_luong>0)?show_hide_btn_up_down("none","block",id,so_luong):(show_hide_btn_up_down("block","none",id,so_luong) & del_buy(id,index));
  save_Storage(list_buy);
}
function del_buy(id,index){
  list_buy.splice(document.querySelector(`#card_container #id_${id} #stt`).innerHTML*1,1);
  render(list_buy,item_buy,"card_container");
  show_hide_btn_up_down("block","none",id,0);
  save_Storage(list_buy);
}
function filter() {
  let check = document.getElementById("boloc").value;
  product_list.forEach((item, index) => {
    (item.type == check) ? document.querySelectorAll(".product_list section")[index].style.display = "block" : document.querySelectorAll(".product_list section")[index].style.display = "none";
  })
  if (check == "All") render(product_list, item_product, "product_list");
}
function show_table_list_buy() {
  document.querySelector(".card").style.display = "block";
  document.querySelector(".card").classList.add("card_show");
  document.getElementById("total").style.top=`${((!document.querySelectorAll(`#card_container .card_row`)[0])?10:document.querySelectorAll(`#card_container .card_row`)[0].offsetHeight)*(list_buy.length+0.5)}px`;// đừng care dòng này :v
//  ( list_buy.length>0)?document.getElementById("total").style.display="flex":document.getElementById("total").style.display="none";
document.getElementById("total").style.display="flex";
 (!document.querySelector(`#span_total`))?true:document.querySelector(`#span_total`).innerHTML =tong().tong_price ;//show tong gia tien 
  document.querySelectorAll(".card_container #icon_buy").forEach((item) => {  
    item.style.display = "block"; // icon tang giam
  })
  document.querySelector("#close_list_buy").addEventListener("click", () => {// nut close man hinh danh sach chon mua
    document.querySelector(".card").style.display = "none";
  })
}
function show_hide_btn_up_down(display_add,display_up_down,id,so_luong){
  document.getElementById("thongbao_sl").innerHTML=tong().tong_sl;
  document.querySelector(`.product_list #id_${id} .fa-shopping-basket`).style.display = display_add;
  document.querySelector(`.product_list #id_${id} #icon_buy`).style.display = display_up_down;
  (!document.querySelector(`.product_list #id_${id} #soluong`))?true:document.querySelector(`.product_list #id_${id} #soluong`).innerHTML = so_luong; //show so luong sau khi thay doi
  (!document.querySelector(`.card_container #id_${id} #soluong`))?true:document.querySelector(`.card_container #id_${id} #soluong`).innerHTML = so_luong;
  (!document.querySelector(`.card_container #id_${id} #price`))?true:document.querySelector(`.card_container #id_${id} #price`).innerHTML = list_buy[document.querySelector(`.card_container #id_${id} #stt`).innerHTML*1].product.price*so_luong;//show gia tien sau khi thay doi so luong
  (!document.querySelector(`#span_total`))?true:document.querySelector(`#span_total`).innerHTML =tong().tong_price ;//show tong gia tien
  document.getElementById("total").style.top=`${((!document.querySelectorAll(`#card_container .card_row`)[0])?10:document.querySelectorAll(`#card_container .card_row`)[0].offsetHeight)*(list_buy.length+0.75)}px`;// đừng care dòng này :v
}
function thanh_toan(){
  list_buy=[];
  save_Storage(list_buy);
  render(list_buy,item_buy,"card_container");
  render(product_list,item_product,"product_list")
  document.getElementById("total").style.display="none"
  let tong=0;
    list_buy.forEach((item)=>{ tong+= item.index; });
    document.getElementById("thongbao_sl").innerHTML= tong;
    document.querySelector(".card").style.display = "none";
}
function tong(){
  let tong_sl=0;
  let tong_price=0;
  list_buy.forEach((item)=>{ 
   tong_sl+= item.index;
   tong_price+=(item.product.price*1)*item.index;
   });
  
return{
 tong_sl,
 tong_price,
}
};